
from obspy import read
from obspy.clients.fdsn import Client
from obspy.core import AttribDict
from obspy.geodetics import gps2dist_azimuth
from obspy import Stream

import xml.dom.minidom

import matplotlib.pyplot as plt 
from matplotlib.transforms import blended_transform_factory
import numpy as np  
from collections import OrderedDict

import os
import sys
import logging

from adjustText import adjust_text

servidor_fdsn='192.168.1.35'
port=8080
number_stations2plot=10


def load_mseed_files(scwf_event_path,station_set):   
    """
    Read mseed files created by scwfparam and returns it as a list of streams if they're in a station_set. 
    
    :param string scwf_event_path: path to the folder created by scwfparam
    :returns: python list of obspy stream objects.
    :raises Exception e: Log if couldn't read a MSEED file. 
    """
    
    stream_list=[]
    mseed_files=os.listdir(scwf_event_path)
    for f in mseed_files:
        
        if "_BP" in f:
            try:
                temp_stream=read(os.path.join(scwf_event_path,f))
                if temp_stream[0].stats.station in station_set:
                    stream_list.append(temp_stream)
                
            except Exception as e:
                logging.info("Not an mseed file: %s, %s" %(f,str(e))) 
        else:
            logging.info("Bad file: %s. Check the station in inventory" %f)
    
    return stream_list



def attach_coordinates(fdsn_client,trace ):
    """
    Add coordinates info got from FDSN server as an atribDict in a trace object.  
    
    :param obspy.fdsn.client fdsn_client: client to connect to a FDSN server
    :param obspy.trace trace: waveform data
    :returns: obspy.trace with latitude, longitude and elevation attached as coordinates dict. 
    :raises Exception e: Log if fails to get data  
    """
    try:
        station_info=fdsn_client.get_stations(network=trace.stats.network,station=trace.stats.station,channel=trace.stats.channel,level="channel",format=None)       
        trace.stats.coordinates=AttribDict({'latitude':station_info[0][0][0].latitude,'longitude':station_info[0][0][0].longitude,'elevation':station_info[0][0][0].elevation})
        
        return trace
    
    except Exception as e:
        logging.info("Fail to get data for %s. Error was: %s" %(trace,str(e)))


def attach_peak_values(trace):
    """
    Add maximum values pga and pgv as an atribDict in trace.   
    
    :param obspy.trace trace: data trace of a station
    :returns: obspy.trace with pga and pgv attached as part of smparam dict. 
    """
    try:
        
        trace.stats.smparam=AttribDict()
        trace.stats.smparam['pga']=np.abs(trace.max())
        
        temp_trace=trace.copy()
        trace.stats.smparam['pgv']=np.abs((temp_trace.integrate()).max())
        
        return trace
    except Exception as e:
        logging.info("Error in attach_peak_values() for: %s. Error was: %s" %(trace.stats.station, str(e)))


def attach_distance(trace, event):
    """
    Attach distance to event as an atribDict in trace
    
    :param obspy.trace trace: obspy trace object
    :param obspy.event event: obspy event object 
    :returns obspy.trace attached with distance parameter
    """
    
    distance=gps2dist_azimuth(trace.stats.coordinates.latitude,trace.stats.coordinates.longitude \
                              , event.preferred_origin().latitude, event.preferred_origin().longitude)
    
    trace.stats['distance']=distance[0]
    return trace

def connect_fdsn(servidor_fdsn,port):
    """
    Connect to a FDSNWS server
    
    :param string servidor_fdsn: Hostname or IP of the FDSN server
    :param int port: Port number to connect to. 
    :returns obspy.clients.fdsn 
    :raises Exception e: Log if the connection couldn't be stablished and exit. 
    """
    logging.info("Connect to FDSNWS")
    try:
        client=Client("http://%s:%s" %(servidor_fdsn,port))
        return client
    except Exception as e:
        logging.info("Error while connecting to FDSN: %s,%s. Error was: %s" %(servidor_fdsn,port,str(e)))
        raise Exception("Error while connecting to FDSN: %s,%s. Error was: %s" %(servidor_fdsn,port,str(e)))

def get_event(fdsn_client,eventID):
    """
    Recover and obspy.event object using sc3 eventID
    
    :param obspy.clients.fdsn fdsn_client: 
    :param string eventID: event identifier in SC3 format. 
    :returns obspy.event 
    :raises Exception e: Log if the event couldn't be recovered and exit. 
    """
    logging.info("Get information of the event: %s" %eventID)
    try:
        catalog=fdsn_client.get_events(eventid=eventID,includearrivals=True)
        event= catalog[0]
        return event
    
    except Exception as e:
        logging.info("Error in get_event_info(). Cannot recover event: %s . Error was: %s" %(eventID,str(e)))
        raise Exception("Error in get_event_info(). Cannot recover event: %s . Error was: %s" %(eventID,str(e)))

def create_station_set(stream_list):
    """
    Create a python set of stations
    
    :param python.list stream_list: list of streams
    :returns python set 
    """
    
    logging.info("Create a set of stations")
    station_list=[]
    for stream in stream_list:
        station_list.append(stream[0].stats.station)
    station_set=set(station_list)
    return station_set

def create_station_set_from_xml(event_dat_file_path):
    """
    Create a set from event_dat.xml
    
    :param string event_dat_file_path : path to event_dat.xml file created by scwfparam
    :returns python.set  
    """
    
    logging.info("Create a list of stream based on a set")
    
    station_list_temp=[]
    doc=xml.dom.minidom.parse(event_dat_file_path)
    station_list=doc.getElementsByTagName("station")
    
    for station in station_list:
        station_list_temp.append(station.getAttribute("code"))
    station_set=set(station_list_temp)
    return station_set



def add_extra_parameters(fdsn_client,stream_list,event):
    """
    Add peak values, coordinates and distance to each stream in a list
    
    :param python.list stream_list: list of streams
    :param obspy.event event: event object 
    :returns list of traces with extra parameters. 
    """
    
    logging.info("Add extra parameters like distance, pga, etc.")
    
    trace_list=[]
    for stream in stream_list:
        trace_temp=stream[0]
        trace_temp=attach_coordinates(fdsn_client,trace_temp)
        trace_temp=attach_peak_values(trace_temp)
        trace_temp=attach_distance(trace_temp, event) 
        trace_list.append(trace_temp)
        
    return trace_list

 
def order_tracelist_by_pga(trace_list,station_set):
    """
    Return a trace list ordered by max PGA value
    
    :param obspy.trace trace_list: list of traces
    :param set station_set: set of unique and valid stations names 
    :returns python list of traces. 
    """
    
    logging.info("Order trace by PGA")
    
    station_pga_dict={}
    for trace in trace_list:
        station_pga_dict.update({"%s-%s" %(trace.stats.station,trace.stats.channel):trace.stats.smparam['pga']})
    
    station_max_pga_dict={}    
    
    channel=['HNE','HNN','HNZ']
    for station_name in station_set:
        temp_list=[]
        for chan in channel:
            temp_key='%s-%s' %(station_name,chan)
            if temp_key in station_pga_dict:
                temp_list.append(station_pga_dict[temp_key])
        if len(temp_list)!=0:     
            max_index=np.argmax(temp_list)
            station_max_pga_dict.update({'%s-%s' %(station_name,channel[max_index]):temp_list[max_index]})    

    temp_list=[]
    trace_by_pga=[]
    for key in sorted(station_max_pga_dict,key=station_max_pga_dict.get,reverse=True):
        temp_list.append(key)

    for station_channel in temp_list:
        station=(station_channel.split('-')[0])
        channel=(station_channel.split('-')[1])     

        for trace in trace_list:
            if trace.stats.station==station and trace.stats.channel==channel:
                trace_by_pga.append(trace)    
    
    return trace_by_pga


def order_trace_list_by_distance(trace_list,plot_channel):
    """
    Return a trace list ordered by distance to an event
    
    :param list trace_list: list of obspy.traces
    :param string plot_channel: channel's name to plot
    :returns ordered trace_list
    """
    
    logging.info("Order traces by distance to the event.")
    
    station_dist_dict={}
    for trace in trace_list:
        station_dist_dict.update({"%s" %trace.stats.station : trace.stats.distance})
    
    temp_list=[]
    for key in sorted(station_dist_dict,key=station_dist_dict.get,reverse=False):
        temp_list.append(key)
    
    trace_by_distance=[]
    for station in temp_list:
        for trace in trace_list:
            if trace.stats.station==station and trace.stats.channel==plot_channel:
                trace_by_distance.append(trace)

    return trace_by_distance


def plot_peak_centered(trace_list_bypga):
    """
    Plot the data ordered by peak 
    """
    figure,axes=plt.subplots(len(trace_list_bypga),1,sharex=True,sharey=True)
    
    number_stations=10
    low_win=50
    up_win=100
    
    for j in range(0,number_stations):
        trace=trace_list_bypga[j]
        plt.subplot(number_stations,1,j+1)
        i_max=np.argmax(trace.data)
        peak_time=trace.stats.starttime + i_max*trace.stats.delta
        low_time= peak_time - low_win
        up_time= peak_time + up_win
        
        trace=trace.slice(low_time , up_time)
        x_range=np.arange(0,trace.stats.npts*trace.stats.delta,trace.stats.delta)
        try:
            plt.plot(x_range,trace.data,'k',linewidth=0.5)
            plt.text(x_range[10],0.3*trace.stats.smparam['pga'],"%s %s %s " %(trace.stats.network,trace.stats.station,trace.stats.channel) )
            plt.text(x_range[-400],0.3*trace.stats.smparam['pga'], "%.2f cm/s**2" %trace.stats.smparam['pga'])
        except Exception as e:
            logging.info("Error was %s" %str(e))
            
    figure.subplots_adjust(hspace=0)
    
    plt.setp([a.get_xticklabels() for a in figure.axes[:-1]], visible=False)
    #plt.setp([a.get_yticklabels() for a in figure.axes[:]], visible=False)
    #plt.show()


def plot_distance_ordered(trace_list,event,number_stations2plot,output_dir,max_trace):
    """
    Plot traces ordered by distance to event
    
    :param list trace_list: list of obspy.trace objects
    :param obspy.event event: event object 
    """
    
    logging.info("Plot the traces")
    
    plt.rcParams["figure.figsize"]=[16,9]
    if number_stations2plot >= len(trace_list):
        number_stations2plot=len(trace_list)
    
    figure,axes=plt.subplots()   
    
    '''
    TODO: Choose these values according to a table with magnitudes 
    '''
    low_win=50
    up_win=150
    
    trace_sep= abs(np.max(max_trace.data)*0.8)
    event_time=event.preferred_origin().time
    event_id=event.resource_id.id.split("/")[2]
    latitude=event.preferred_origin().latitude
    longitude=event.preferred_origin().longitude
    depth= event.preferred_origin().depth 
    mag_val="%.2f" %event.preferred_magnitude().mag
    mag_type=event.preferred_magnitude().magnitude_type
    event_datetime=event.preferred_origin().time.strftime("%Y-%m-%d %H:%M:%S")
    
    for j in range(0,number_stations2plot):
        
        trace=trace_list[j]
        trace=trace.slice(event_time - low_win ,  event_time + up_win)
        x_range=np.arange(0,trace.stats.npts*trace.stats.delta,trace.stats.delta)
        margin_x=int(round(0.05*(x_range.size)))
        
        try:
            axes.plot(x_range,trace.data - j*trace_sep, 'k', linewidth=0.5)
            plt.text(x_range[margin_x], -j*trace_sep + 0.1*trace_sep  ,"%s %s %s, %.2f km " %(trace.stats.network,trace.stats.station,trace.stats.channel,trace.stats.distance/1000) )
            #plt.text(x_range[margin_x],-j*trace_sep , "distancia hipocentral: %.2f km" %(trace.stats.distance/1000)  )
            #plt.text(0, 0 ,"%s %s %s " %(trace.stats.network,trace.stats.station,trace.stats.channel) )
            #plt.text(0,0, "distancia hipocentral: %.2f km" %(trace.stats.distance/1000)  )        
            plt.text(x_range[-margin_x],-j*trace_sep + 0.1*trace_sep , ' $%.2f  cm/s^2$' %(float(trace.stats.smparam['pga'])*100))
            
        except Exception as e:
            logging.info("Error was %s" %str(e))
            
    plt.setp([a.get_yticklabels() for a in figure.axes[:]], visible=False)
    plt.setp([a.tick_params(length=0) for a in figure.axes[:]])
    plt.xlabel("Tiempo (s)")

    figure.suptitle("%s:  %s UTC ,  %s %s, lat:%.2f, lon:%.2f, prof:%.2f km" %(event_id,event_datetime,mag_val, mag_type,latitude,longitude,depth/1e3) )
    plt.savefig( "%s/%s" %(output_dir, "PGA_distance.png") ,figsize=(1900,800), dpi=96)
    #plt.show()


def plot_as_section(trace_bydist):
    
    stream_bypga=Stream()
    for trace in trace_bydist:
        stream_bypga+=trace
    
    fig = plt.figure()
    stream_bypga.plot(type='section',orientation='horizontal',show=False, fig=fig)
    ax=fig.axes[0]
    for tr in stream_bypga:
        ax.text(10.0,tr.stats.distance/1e3,tr.stats.station,rotation=0,
                va="bottom", ha="center",zorder=10)
    
    #plt.show()


def text_pga(trace_list_complete,trace_ordered,number_stations2plot,event,output_dir):
    
    """
    This function prints ordered stations's PGA values for each channel 
    """    
    
    pga_file=open("%s/%s" %(output_dir,"pga.txt"),"w+")
    
    if number_stations2plot >= len(trace_ordered):
        number_stations2plot = len(trace_ordered)
    
    station_dict=create_dictionary(trace_list_complete)
    
    #pga_file.write("Station, dist(km) PGA(cm*s2) E, N, Z\n")
    print("Station, dist(km) PGA(cm*s2) E, N, Z")
    
    for tr in trace_ordered:
        
        tr_e,tr_n,tr_z=(station_dict['%s-HNE' %tr.stats.station],station_dict['%s-HNN' %tr.stats.station],station_dict['%s-HNZ' %tr.stats.station])
        
        '''
        print("%s , %.2f, %.2f, %.2f, %.2f " %( tr.stats.station,tr.stats['distance']/1000,\
                                          tr_e.stats.smparam['pga']*100,tr_n.stats.smparam['pga']*100,tr_z.stats.smparam['pga']*100))
        '''
        
        pga_file.write("%s , %.2f, %.2f, %.2f, %.2f \n" %( tr.stats.station,tr.stats['distance']/1000,\
                                          tr_e.stats.smparam['pga']*100,tr_n.stats.smparam['pga']*100,tr_z.stats.smparam['pga']*100))
        
        #print(tr_e.stats.smparam['pga'],tr_n.stats.smparam['pga'],tr_z.stats.smparam['pga'])

def plot_pga_vs_distance(trace_list,number_stations2plot,event,output_dir):
    
    #number_stations2plot=100
    event_id=event.resource_id.id.split("/")[2]
    latitude=event.preferred_origin().latitude
    longitude=event.preferred_origin().longitude
    depth= event.preferred_origin().depth 
    mag_val="%.2f" %event.preferred_magnitude().mag
    mag_type=event.preferred_magnitude().magnitude_type
    event_datetime=event.preferred_origin().time.strftime("%Y-%m-%d %H:%M:%S")
    
    if number_stations2plot >= len(trace_list):
        number_stations2plot=len(trace_list)
    
    fig2,ax2=plt.subplots()
    
    station_pga=[]
    station_name=[]
    station_distance=[]
    
    for trace in trace_list[0:number_stations2plot]:
        station_pga.append(trace.stats.smparam['pga']*100)
        station_distance.append(trace.stats.distance/1000)
        station_name.append("%s %s" %(trace.stats.station,trace.stats.channel))
    
    ax2.plot(station_distance,station_pga,'ko:',linewidth=0.25)
    #plt.xticks(station_distance,station_name,rotation="vertical")
    #fig2.autofmt_xdate()
    plt.ylabel("PGA ($cm/s^2$)")
    plt.xlabel("Distance (km)")
    fig2.suptitle("%s:  %s UTC ,  %s %s, lat:%.2f, lon:%.2f, prof:%.2f km" %(event_id,event_datetime,mag_val, mag_type,latitude,longitude,depth/1e3) )

    texts = [plt.text(station_distance[i],station_pga[i],
                      "%s\n (%.2f, %.2f)" %(name,station_distance[i],station_pga[i]),
                      ha="center",va="center") for i,name in enumerate(station_name)]
    
    adjust_text(texts,arrowprops=dict(arrowstyle="fancy", color='red'))
            
    plt.savefig( "%s/pga_vs_distance.png" %output_dir ,figsize=(1900,800), dpi=96)

def create_dictionary(trace_list):
    """
    Create a python dict usign a list as input. 
    """
    
    station_dict={}
    
    for trace in trace_list: 
        station_dict.update({"%s-%s" %(trace.stats.station, trace.stats.channel): trace})
        
    return station_dict


def trace_station_pga(trace_list,trace_ordered,number_stations2plot,evento, output_dir):
    """
    This function creates a subplot for each station and channel 
    """
    '''
    TODO: Choose these values according to a table with magnitudes 
    '''

    low_win=50
    up_win=150
    station_dict=create_dictionary(trace_list)
    
    event_time=evento.preferred_origin().time
    plt.rcParams["figure.figsize"]=[16,9]
    
    if number_stations2plot >= len(trace_ordered):
        number_stations2plot = len(trace_ordered)
    
    for i,tr in enumerate(trace_ordered[0:number_stations2plot]):
        trace_list_temp=[]
        
        try:
            trace_list_temp.append(station_dict['%s-HNE' %tr.stats.station])
            trace_list_temp.append(station_dict['%s-HNN' %tr.stats.station])
            trace_list_temp.append(station_dict['%s-HNZ' %tr.stats.station])
        
        except Exception as e:
            print("Error in trace_station_pga was %s" %str(e))

        figure,axes=plt.subplots(3,1,sharex=True,sharey=True)
    
        for j,trace in enumerate(trace_list_temp):
            
            trace=trace.slice(event_time - low_win, event_time + up_win)
            trace_sep=abs(trace.max()*0.8)
            try:
                
                plt.subplot(3,1,j+1)
                x_range=np.arange(0,trace.stats.npts*trace.stats.delta,trace.stats.delta)
                margin_x=int(round(0.1*(x_range.size)))
                plt.plot(x_range,trace.data,'k',linewidth=0.25)
                
                plt.text(x_range[margin_x], 0.1*trace_sep ,"%s %s " %(trace.stats.station,trace.stats.channel))
                plt.text(x_range[margin_x],-0.15*trace_sep ,"$PGA= %.2f cm/s^2 $" %(trace.stats.smparam['pga']*100))
                plt.xlabel("Tiempo (s)")
                           
            except Exception as e:
                print("Error in plot was: %s" %str(e))
                continue
            
        mag_val="%.2f" %evento.preferred_magnitude().mag
        mag_type=evento.preferred_magnitude().magnitude_type
        event_datetime=evento.preferred_origin().time.strftime("%Y-%m-%d %H:%M:%S:")
        figure.suptitle("%s %s %s" %(event_datetime,mag_val, mag_type) )
        figure.subplots_adjust(hspace=0)
        plt.savefig( "%s/%s_%s_pga.png" %(output_dir,i, trace.stats.station ) ,figsize=(1900,800), dpi=96)
        #plt.show()


def main():
    """
    This method check input parameters and call the rest of the functions:
    
        Connect to FDSN 
        Get information from the event
        Create a set of stations to work with. 
        Create a list of stream based on a set
        Add extra parameters like distance, pga, etc. 
        Order trace by PGA
        Get the channgel of the maximum PGA
        Order traces by distance to the event. 
        Plot each traces PGA 
        Create text PGA.  
        
    """        

    is_error=False
         
    if len(sys.argv)==1 or len(sys.argv) != 6:
        is_error=True
    
    else:
       
        scwf_event_path=sys.argv[1]
        xml_event_file=sys.argv[2]
        eventID =      sys.argv[3]
        output_dir =     sys.argv[4]
        LOGFILE = sys.argv[5]
       
        logging.basicConfig(filename=LOGFILE,format='%(asctime)s %(levelname)s %(message)s',level=logging.INFO)
        
        logging.info("Script parameters: %s" %sys.argv)

        fdsn_cl=connect_fdsn(servidor_fdsn,port)
        
        evento=get_event(fdsn_cl,eventID)
                    
        station_set=create_station_set_from_xml(xml_event_file)
        
        stream_list=load_mseed_files(scwf_event_path,station_set)
        
        trace_list=add_extra_parameters(fdsn_cl,stream_list, evento)
        
        trace_bypga=order_tracelist_by_pga(trace_list,station_set)
             
        max_pga_channel=trace_bypga[0].stats.channel
        
        max_trace=trace_bypga[0]
        
        trace_bydist=order_trace_list_by_distance(trace_list,max_pga_channel)
        
        plot_distance_ordered(trace_bydist, evento,number_stations2plot,output_dir,max_trace)
        #plot_as_section(trace_bydist)
        #plot_peak_centered(trace_bypga)
        ###ADD FUNCTION TO CREATE IMAGE ACC VS DISTA 
        plot_pga_vs_distance(trace_bydist,number_stations2plot,evento,output_dir)
        
        trace_station_pga(trace_list, trace_bydist,number_stations2plot, evento, output_dir)
        
        text_pga(trace_list,trace_bydist, number_stations2plot, evento, output_dir)
        
    if is_error:
        logging.info(f"Usage: python {sys.argv[0]} scwf_waveform_dir event_id/event_dat.xml sc3_event_id output_directory log_file_path")
        print(f"Usage: python {sys.argv[0]} scwf_waveform_dir event_id/event_dat.xml sc3_event_id output_directory log_file_path")

main()





        


